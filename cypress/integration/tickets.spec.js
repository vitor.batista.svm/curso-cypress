
describe("Tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    it("Fills all the text input fields", () => {
        const firstName = "Vitor";
        const lastName = "Batista";
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("vitor.batista.svm@gmail.com");
        cy.get("#requests").type("Vegetariano");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("select rwo tickets", () =>{
        cy.get("#ticket-quantity").select("2");

    });
    it("select radion button VIP", () => {
        cy.get("#vip").check();
    });
    it("checkbox social-media", () =>{
        cy.get("#social-media").check();

    });
    
    it("selects friends, and publication, the unchecked friends ", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();

    });
    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain","TICKETBOX");

    });
    it("Alerts on invalid email", ()=>{
        cy.get("#email")
            .as("email")
            .type("talkingabouttesting-gmail.com");

        cy.get("#email.invalid").should("exist");
            

        cy.get("@email")
            .clear()
            .type("talkingabouttesting@gmail.com")

            cy.get("#email.invalid").should("not.exist")
    });

    it("Fills and reset the form", () =>{
        
        const firstName = "Vitor";
        const lastName = "Batista";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("vitor.batista.svm@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("IPA beer");

        cy.get(".agreement p").should(
            "contain", 
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );
        
        cy.get("#agree").click();
        cy.get("#signature").type(fullName);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled")

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    });

    it("fills mandatory fields using support command", ()=>{
        const customer = {
            firstName:"João",
            lastName:"Silva",
            email:"joaosilva@example.com"
        };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled")

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");
    });


});